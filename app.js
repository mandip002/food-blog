const express= require('express');
const app = express();
const path = require('path');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const upload = require('express-fileupload');
const session = require('express-session');
const passport = require('passport');
const flash = require('connect-flash');




mongoose.connect('mongodb://localhost:27017/cms').then((db)=>{
    console.log('Mongo Connected');
}).catch(error=>console.log(error));


//using static
app.use(express.static(path.join(__dirname, '/public')));

//set view engine




const {generateTime, listItem, myHelper, substr, gHelper} = require('./helpers/handlebars-helpers');

app.engine('handlebars', exphbs({defaultLayout: 'home', helpers: {generateTime: generateTime, listItem: listItem, myHelper:myHelper, substr:substr, gHelper:gHelper}}));
app.set('view engine', 'handlebars');


//Upload Middleware

app.use(upload());

//Body Parser

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

//Method override

app.use(methodOverride('_method'));


app.use(session({
    secret: 'karkimandip4',
    resave: true,
    saveUninitialized: true
}));

app.use(flash());

//PASSPORT

app.use(passport.initialize());
app.use(passport.session());

//Load Routes
const home = require('./routes/home/index');
const admin = require('./routes/admin/index');
const posts = require('./routes/admin/posts');
const restaurants = require('./routes/admin/restaurants');
const gallerys = require('./routes/admin/gallerys');
const adds = require('./routes/admin/adds');
const nodemailer = require('nodemailer');

//load Routes
app.use('/', home);
app.use('/admin', admin);
app.use('/admin/posts', posts);
app.use('/admin/restaurants', restaurants);
app.use('/admin/gallerys', gallerys);
app.use('/admin/adds', adds);


app.post('/', (req,res) =>{
addEmailChimp(req.body.email);
res.redirect('back');
});

function addEmailChimp(email){
    var request = require("request");

var options = { method: 'POST',
  url: 'https://us18.api.mailchimp.com/3.0/lists/24b6e1cb5a/members',
  headers: 
   { 'Postman-Token': '7886d8a5-ea60-4dba-be12-d4ab57d224ea',
     'Cache-Control': 'no-cache',
     Authorization: 'Basic YW55c3RyaW5nOmUyYWNmNGEzZmFmZDJkNzE2YmRmY2Q1MzI5OGUxYzBhLXVzMTg=',
     'Content-Type': 'application/json' },
  body: { email_address: email, status: 'subscribed' },
  json: true };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

}


app.listen(3000, ()=>{
    console.log('listening on port 3000');
    
});