const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');
const Restaurant = require('../../models/Restaurant');
const Gallery = require('../../models/Gallery');
const User = require('../../models/User');

const bcrypt = require('bcryptjs');
const passport = require('passport');
const LocalStrategy  = require('passport-local').Strategy;

router.all('/*', (req,res,next)=>{
    req.app.locals.layout = 'home';
    next();
});




  router.get("/",function(req,res) {
    var posts={}; //Create Empty order Object
    var restaurants={}; //Create Empty product Object
    
    Post.find({}).sort({"date":'desc'}).exec(function (err,allposts) {
        if (err) {
            console.log(err);
        } else {
            //Find Collection And Assign It To Object
            posts=allposts;             
        }
    });
    
    Restaurant.find({}).sort({"date":'desc'}).exec(function(err, allRestaurants) {
        if (err) {
            console.log(err);
        } else {
            //find order collection and sort it in desending order and assign it to order object  
            restaurants=allRestaurants;
            res.render("home/index",{posts:posts , restaurants:restaurants});
        }
    });
  });

/* router.get('/', (req,res)=>{
    Restaurant.find({})
    .sort({date:'desc'})
    .then(restaurants=>{
      res.render('home/index', {restaurants:restaurants});
    });
    });
    */

  
  router.get('/allPost', (req,res)=>{
    Post.find({})
    .sort({date:'desc'})
    .then(posts=>{
      res.render('home/allPost', {posts:posts});
    });
  });

  router.get('/post/:id', (req,res)=>{
    Post.findOne({_id: req.params.id})
    .then(post =>{
      res.render('home/singlePost', {post:post});
    });
   });

   router.get('/posts', (req,res)=>{
    Post.find({})
    .sort({date:'desc'})
    .then(posts=>{
      res.render('home/allPost', {posts:posts});
    });
  });
  module.exports = router;

//left




  router.get('/allRestaurant', (req,res)=>{
    Restaurant.find({})
    .sort({date:'desc'})
    .then(restaurants=>{
      res.render('home/allRestaurant', {restaurants:restaurants});
    });
  });

  router.get('/restaurant/:id', (req,res)=>{
    Restaurant.findOne({_id: req.params.id})
    .then(restaurant =>{
      res.render('home/singleRestaurant', {restaurant:restaurant});
    });
   });

   router.get('/restaurants', (req,res)=>{
    Restaurant.find({})
    .sort({date:'desc'})
    .then(restaurants=>{
      res.render('home/allRestaurant', {restaurants:restaurants});
    });
  });


  router.get('/', (req,res)=>{
    Gallery.find({})
    .sort({date:'desc'})
    .then(gallerys=>{
      res.render('home/index', {gallerys:gallerys});
    });
  }); 
  
      router.get('/allGallery', (req,res)=>{
        Gallery.find({})
        .sort({date:'desc'})
        .then(gallerys=>{
          res.render('home/allGallery', {gallerys:gallerys});
        });
      });
  
      
  router.get('/gallerys', (req,res)=>{
        Gallery.find({})
        .sort({date:'desc'})
        .then(gallerys=>{
          res.render('home/allGallery', {gallerys:gallerys});
        });
      });

      router.get('/game', (req, res)=>{
        res.render('home/game');
      });

     
  router.get('/login', (req,res)=>{
      res.render('home/login');
    });

    passport.use(new LocalStrategy({usernameField: 'email'}, (email,password, done)=>{
     User.findOne({email: email}).then(user=>{
       if(!user) return done(null, false, {message: 'No user found'});
       bcrypt.compare(password, user.password, (err, matched)=>{
        if(err) return err;
        if(matched) {
          return done(null, user);
        } else {
          return done(null, false, {messgae: 'Incorrect password'});
        }
       });
    });
  }));

  router.get('/logout', (req, res)=>{
    req.logOut();
    res.redirect('/login');
  });

  passport.serializeUser(function(user,done){
    done(null, user.id);
  });
  passport.deserializeUser(function(id, done){
    User.findById(id, function(err, user){
      done(err, user);
    });
  });
  

    router.post('/login', (req,res, next)=>{
      passport.authenticate('local', {
        successRedirect: '/admin',
        failureRedirect: '/login'
      })(req, res, next);
    });
  
    router.get('/register', (req,res)=>{
      res.render('home/register');
    });

    router.post('/register', (req,res)=>{
      const newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password
      });

      bcrypt.genSalt(10, (err, salt)=>{
        bcrypt.hash(newUser.password, salt,(err, hash)=>{
         newUser.password = hash;
         newUser.save().then(savedUser=>{
         res.redirect('/login');      
        });

        });
      });
    });

      
        module.exports = router;