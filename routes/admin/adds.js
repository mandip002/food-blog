const express = require('express');
const router = express.Router();
const Add = require('../../models/Add');
const {userAuthenticated} = require('../../helpers/authentication')


router.all('/*', userAuthenticated, (req,res,next)=>{
    req.app.locals.layout = 'admin';
    next();
});

router.get('/', (req,res)=>{
    Add.find({}).then(adds=>{
      res.render('admin/adds/index', {adds: adds});
    }).catch(error=>{
      console.log(error)});
 });

  router.get('/create', (req,res)=>{
    res.render('admin/adds/create');
  });

  router.post('/create', (req,res)=>{

   const newAdd = new Add({
      title: req.body.title
    });

    newAdd.save().then(savedAdd=>{
      res.redirect('/admin/adds');
    }).catch(error =>{
      console.log('could not save post');   
  });
});

router.get('/edit/:id', (req,res)=>{
  Add.findOne({_id: req.params.id}).then(add=>{
    res.render('admin/adds/edit', {add:add});
  });
  
});
  
router.put('/edit/:id', (req,res)=>{
  Add.findOne({_id: req.params.id})
  .then(add=>{
   add.title = req.body.title,
   add.body = req.body.body
  
   add.save().then(updatedAdd=>{
    res.redirect('/admin/adds');
   });
    });
});

router.delete('/:id', (req,res)=>{
  Add.remove({_id:req.params.id})
  .then(result=>{
   res.redirect('/admin/adds');
  });
});

  module.exports = router;