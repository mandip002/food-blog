const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');
const {userAuthenticated} = require('../../helpers/authentication');
const {isEmpty} = require('../../helpers/upload-helper');
var multer  = require('multer')

router.all('/*', userAuthenticated, (req,res,next)=>{
    req.app.locals.layout = 'admin';
    next();
});

router.get('/', (req,res)=>{
    Post.find({})
    .sort({date: 'desc'})
    .then(posts=>{
      res.render('admin/posts/index', {posts: posts});
    }).catch(error=>{
      console.log(error)});
 });

  router.get('/create', (req,res)=>{
    res.render('admin/posts/create');
  });



  router.post('/create',  (req,res,err)=>{
    
  
    if(!isEmpty(req.files)) {
     let file = req.files.file;
    let filename = Date.now() + '-' + file.name;
    file.mv('./public/img/foods/' + filename, (err)=>{      
      if(err) throw err;
    });
  
}
    const newPost = new  Post({
      title: req.body.title,
      body: req.body.body,
      name:req.body.name,
      file: filename,
    });

    newPost.save().then(savedPost=>{
      res.redirect('/admin/posts');
    }).catch(error =>{
      console.log('could not save post');   
  });

}); 

router.get('/edit/:id', (req,res)=>{
  Post.findOne({_id: req.params.id}).then(post=>{
    res.render('admin/posts/edit', {post:post});
  });
  
});
  
router.put('/edit/:id', (req,res)=>{
  Post.findOne({_id: req.params.id})
  .then(post=>{
   post.title = req.body.title,
   post.body = req.body.body,
   post.name = req.body.name
  
   post.save().then(updatedPost=>{
    res.redirect('/admin/posts');
   });
    });
});

router.delete('/:id', (req,res)=>{
  Post.remove({_id:req.params.id})
  .then(result=>{
   res.redirect('/admin/posts');
  });
});

  module.exports = router;