const express = require('express');
const router = express.Router();
const Gallery = require('../../models/Gallery');
const {userAuthenticated} = require('../../helpers/authentication')
const {isEmpty} = require('../../helpers/upload-helper');
router.all('/*', (req,res,next)=>{
    req.app.locals.layout = 'admin';
    next();
});

router.get('/', userAuthenticated,(req,res)=>{
    Gallery.find({}).then(gallerys=>{
      res.render('admin/gallerys/index', {gallerys: gallerys});
    }).catch(error=>{
      console.log(error)});
 });

  router.get('/create', (req,res)=>{
    res.render('admin/gallerys/create');
  });

  router.post('/create', (req,res)=>{
    
  let file = req.files.file;
    let filename = Date.now() + file.name;

    
    
    file.mv('./public/img/gallery/' + filename, (err)=>{
      if(err) throw err;
    });
  
   const newGallery = new Gallery({
      title: req.body.title,
      file : filename
    });

    newGallery.save().then(savedGallery=>{
      res.redirect('/admin/gallerys');
    }).catch(error =>{
      console.log('could not save post');   
  });
});

router.get('/edit/:id', (req,res)=>{
  Gallery.findOne({_id: req.params.id}).then(gallery=>{
    res.render('admin/gallerys/edit', {gallery:gallery});
  });
  
});
  
router.put('/edit/:id', (req,res)=>{
  Gallery.findOne({_id: req.params.id})
  .then(gallery=>{
   gallery.title = req.body.title,
   gallery.body = req.body.body
  
   gallery.save().then(updatedGallery=>{
    res.redirect('/admin/gallerys');
   });
    });
});

router.delete('/:id', (req,res)=>{
  Gallery.remove({_id:req.params.id})
  .then(result=>{
   res.redirect('/admin/gallerys');
  });
});

  module.exports = router;