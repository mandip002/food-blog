const express = require('express');
const router = express.Router();
const Restaurant = require('../../models/Restaurant');
const {userAuthenticated} = require('../../helpers/authentication')

router.all('/*', userAuthenticated, (req,res,next)=>{
    req.app.locals.layout = 'admin';
    next();
});

router.get('/', (req,res)=>{
    Restaurant.find({})
    .sort({date: 'desc'})
    .then(restaurants=>{
      res.render('admin/restaurants/index', {restaurants: restaurants});
    }).catch(error=>{
      console.log(error)});
 });

  router.get('/create', (req,res)=>{
    res.render('admin/restaurants/create');
  });

  router.post('/create', (req,res)=>{
    
    let file = req.files.file;
    let filename = Date.now + file.name;
    
    file.mv('./public/img/restaurants/' + filename, (err)=>{
      if(err) throw err;
    });
  

   const newRestaurant = new Restaurant({
      title: req.body.title,
      body: req.body.body,
      name:req.body.name,
      file: filename
    });

    newRestaurant.save().then(savedRestaurant=>{
      res.redirect('/admin/restaurants');
    }).catch(error =>{
      console.log('could not save post');   
  });
});

router.get('/edit/:id', (req,res)=>{
  Restaurant.findOne({_id: req.params.id}).then(restaurant=>{
    res.render('admin/restaurants/edit', {restaurant:restaurant});
  });
  
});
  
router.put('/edit/:id', (req,res)=>{
  Restaurant.findOne({_id: req.params.id})
  .then(restaurant=>{
   restaurant.title = req.body.title,
   restaurant.body = req.body.body,
   restaurant.name  = req.body.name
  
   restaurant.save().then(updatedRestaurant=>{
    res.redirect('/admin/restaurants');
   });
    });
});

router.delete('/:id', (req,res)=>{
  Restaurant.remove({_id:req.params.id})
  .then(result=>{
   res.redirect('/admin/restaurants');
  });
});

  module.exports = router;