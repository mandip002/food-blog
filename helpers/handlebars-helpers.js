
const moment = require('moment');
module.exports = {

generateTime: function(date, format){
    return moment(date).format(format);
},
listItem: function(from, to, context, options) {
    var item = "";
    for (var i = from, j = to; i < j; i++) {
        item = item + options.fn(context[i]);
}
},


myHelper : function(object, oBlock){
    var sResult = '';
    
    for(var i=0; i<3; i++)
        sResult += oBlock.fn(object[i]);
    return sResult;

},
gHelper : function(object, oBlock){
    var mResult = '';
    
    for(var i=0; i<1; i++)
        mResult += oBlock.fn(object[i]);
    return mResult;

},

substr : function(length, context, options) {
    if ( context.length > length ) {
     return context.substring(0, length) + "...";
    } else {
     return context;
    }
}
   
   
};


