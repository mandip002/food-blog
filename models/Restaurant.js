const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RestaurantSchema = new Schema ({

    title: {
       type: String,
       require: true
    },

     body: {
        type: String,
        require: true
     }, 
     name: {
         type:String,
     },
     file: {
         type: String,
     },
     date: {
        type: Date,
        default: Date.now()
       }
});

module.exports = mongoose.model('Restaurant', RestaurantSchema);