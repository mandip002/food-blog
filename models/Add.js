const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AddSchema = new Schema ({

    title: {
       type: String,
       require: true
    }
});

module.exports = mongoose.model('Add', AddSchema);