const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GallerySchema = new Schema ({

    title: {
       type: String,
       require: true
    },
    file: {
        type: String,
        
    },
    
    date: {
        type: Date,
        default: Date.now()
       }
});

module.exports = mongoose.model('Gallery', GallerySchema);